package config

import (
	"github.com/dgrijalva/jwt-go"
)

func Decodejwt(tokens string) interface{} {
	tokenString := tokens
	claims := jwt.MapClaims{}
	_, err := jwt.ParseWithClaims(tokenString, claims, func(token *jwt.Token) (interface{}, error) {
		return []byte("secret"), nil
	})
	if err != nil {
		res := make(map[string]interface{})
		res["status"] = false
		res["data"] = err.Error()
		return res
	}
	// ... error handling
	// do something with decoded claims
	res := make(map[string]interface{})
	for key, val := range claims {

		res[key] = val
	}

	res1 := make(map[string]interface{})
	res1["status"] = true
	res1["data"] = res["user_id"]
	return res1

}
