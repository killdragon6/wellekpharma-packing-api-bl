package handler

import (
	"main/model"
	"net/http"

	_ "github.com/go-sql-driver/mysql"
	"github.com/labstack/echo"
)

// Admin customer
func Addbil(c echo.Context) error {
	res := model.Addbil(c)
	return c.JSON(http.StatusOK, res)
}

func Getbil(c echo.Context) error {
	res := model.Getbil(c)
	return c.JSON(http.StatusOK, res)
}

func Getbilbyid(c echo.Context) error {
	res := model.Getbilbyid(c)
	return c.JSON(http.StatusOK, res)
}

func Updatebil(c echo.Context) error {
	res := model.Updatebil(c)
	return c.JSON(http.StatusOK, res)
}
func Addqc_pending_checklist(c echo.Context) error {
	res := model.Addqc_pending_checklist(c)
	return c.JSON(http.StatusOK, res)
}
func Deletebill(c echo.Context) error {
	res := model.Deletebill(c)
	return c.JSON(http.StatusOK, res)
}
