package model

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"log"
	"unicode/utf8"

	"github.com/labstack/echo"
)

var DB *sql.DB

func Initialize() {
	db, err := sql.Open("mysql", "root:@/wellekpharma_packing_system")
	if err != nil {
		log.Fatal(err)
	} else {
		fmt.Printf("connectDB")
	}
	DB = db
}
func Addbil(c echo.Context) interface{} {
	json_map := make(map[string]interface{})
	err := json.NewDecoder(c.Request().Body).Decode(&json_map)
	Initialize()
	// json_map := make(map[string]interface{})

	var array []interface{}
	// query :=
	rows, err := DB.Query("INSERT INTO bill(`transmission_line`, `bill_number`, `customer_id`, `customer_name`, `date_sent`, `day_count`, `employee_name`, `create_date`, `create_at`, `modify_date`, `modify_at`, `transmission_details`, `employee_id`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", json_map["transmission_line"], json_map["bill_number"], json_map["customer_id"], json_map["customer_name"], json_map["date_sent"], json_map["day_count"], json_map["employee_name"], json_map["create_date"], json_map["create_at"], json_map["modify_date"], json_map["modify_at"], json_map["transmission_details"], json_map["employee_id"])
	if err != nil {
		panic(err)
	}
	defer rows.Close()

	for rows.Next() {
		var id int
		var transmission_line string
		var bill_number string
		var customer_id string
		var customer_name string
		var date_sent string
		var day_count string
		var employee_name string
		var create_date string
		var create_at string
		var modify_date string
		var modify_at string
		var transmission_details string
		var employee_id string

		rows.Scan(&id,
			&transmission_line,
			&bill_number,
			&customer_id,
			&customer_name,
			&date_sent,
			&day_count,
			&employee_name,
			&create_date,
			&create_at,
			&modify_date,
			&modify_at,
			&transmission_details,
			&employee_id,
		)
		elements := make(map[string]interface{})
		elements["id"] = id
		elements["transmission_line"] = transmission_line
		elements["bill_number"] = bill_number
		elements["customer_id"] = customer_id
		elements["customer_name"] = customer_name
		elements["date_sent"] = date_sent
		elements["day_count"] = day_count
		elements["employee_name"] = employee_name
		elements["create_date"] = create_date
		elements["create_at"] = create_at
		elements["modify_date"] = modify_date
		elements["modify_at"] = modify_at
		elements["transmission_details"] = transmission_details
		elements["employee_id"] = employee_id
		array = append(array, elements)
	}

	res := make(map[string]interface{})
	res["data"] = array
	return res["data"]
}
func Getbil(c echo.Context) interface{} {
	json_map := make(map[string]interface{})
	err := json.NewDecoder(c.Request().Body).Decode(&json_map)
	Initialize()
	// json_map := make(map[string]interface{})

	var array []interface{}
	// query :=
	rows, err := DB.Query("SELECT * FROM bill")
	if err != nil {
		panic(err)
	}
	defer rows.Close()

	for rows.Next() {
		var id int
		var transmission_line string
		var bill_number string
		var customer_id string
		var customer_name string
		var date_sent string
		var day_count string
		var employee_name string
		var create_date string
		var create_at string
		var modify_date string
		var modify_at string
		var transmission_details string
		var employee_id string

		rows.Scan(&id,
			&transmission_line,
			&bill_number,
			&customer_id,
			&customer_name,
			&date_sent,
			&day_count,
			&employee_name,
			&create_date,
			&create_at,
			&modify_date,
			&modify_at,
			&transmission_details,
			&employee_id,
		)
		elements := make(map[string]interface{})
		elements["id"] = id
		elements["transmission_line"] = transmission_line
		elements["bill_number"] = bill_number
		elements["customer_id"] = customer_id
		elements["customer_name"] = customer_name
		elements["date_sent"] = date_sent
		elements["day_count"] = day_count
		elements["employee_name"] = employee_name
		elements["create_date"] = create_date
		elements["create_at"] = create_at
		elements["modify_date"] = modify_date
		elements["modify_at"] = modify_at
		elements["transmission_details"] = transmission_details
		elements["employee_id"] = employee_id
		array = append(array, elements)
	}

	res := make(map[string]interface{})
	res["data"] = array
	return res["data"]
}
func Getbilbyid(c echo.Context) interface{} {
	json_map := make(map[string]interface{})
	err := json.NewDecoder(c.Request().Body).Decode(&json_map)
	Initialize()
	// json_map := make(map[string]interface{})

	var array []interface{}
	// query :=
	rows, err := DB.Query("SELECT * FROM bill  where id = ?", json_map["id"])
	if err != nil {
		panic(err)
	}
	defer rows.Close()

	for rows.Next() {
		var id int
		var transmission_line string
		var bill_number string
		var customer_id string
		var customer_name string
		var date_sent string
		var day_count string
		var employee_name string
		var create_date string
		var create_at string
		var modify_date string
		var modify_at string
		var transmission_details string
		var employee_id string

		rows.Scan(&id,
			&transmission_line,
			&bill_number,
			&customer_id,
			&customer_name,
			&date_sent,
			&day_count,
			&employee_name,
			&create_date,
			&create_at,
			&modify_date,
			&modify_at,
			&transmission_details,
			&employee_id,
		)
		elements := make(map[string]interface{})
		elements["id"] = id
		elements["transmission_line"] = transmission_line
		elements["bill_number"] = bill_number
		elements["customer_id"] = customer_id
		elements["customer_name"] = customer_name
		elements["date_sent"] = date_sent
		elements["day_count"] = day_count
		elements["employee_name"] = employee_name
		elements["create_date"] = create_date
		elements["create_at"] = create_at
		elements["modify_date"] = modify_date
		elements["modify_at"] = modify_at
		elements["transmission_details"] = transmission_details
		elements["employee_id"] = employee_id
		array = append(array, elements)
	}

	res := make(map[string]interface{})
	res["data"] = array
	return res["data"]
}
func Updatebil(c echo.Context) interface{} {
	json_map := make(map[string]interface{})
	err := json.NewDecoder(c.Request().Body).Decode(&json_map)
	Initialize()

	// json_map := make(map[string]interface{})

	var array []interface{}
	var stringupdate string
	if json_map["bill_number"] != nil {
		stringupdate += "bill_number = '" + json_map["bill_number"].(string) + "',"
	}
	if json_map["transmission_line"] != nil {
		stringupdate += "transmission_line = '" + json_map["transmission_line"].(string) + "',"
	}
	if json_map["transmission_details"] != nil {
		stringupdate += "transmission_details = '" + json_map["transmission_details"].(string) + "',"
	}
	if json_map["customer_id"] != nil {
		stringupdate += "customer_id = '" + json_map["customer_id"].(string) + "',"
	}
	if json_map["customer_name"] != nil {
		stringupdate += "customer_name = '" + json_map["customer_name"].(string) + "',"
	}
	if json_map["employee_id"] != nil {
		stringupdate += "employee_id = '" + json_map["employee_id"].(string) + "',"
	}
	if json_map["employee_name"] != nil {
		stringupdate += "employee_name = '" + json_map["employee_name"].(string) + "',"
	}
	if json_map["date_sent"] != nil {
		stringupdate += "date_sent = '" + json_map["date_sent"].(string) + "',"
	}
	if json_map["day_count"] != nil {
		stringupdate += "day_count = '" + json_map["day_count"].(string) + "',"
	}
	if json_map["create_date"] != nil {
		stringupdate += "create_date = '" + json_map["create_date"].(string) + "',"
	}
	if json_map["create_at"] != nil {
		stringupdate += "create_at = '" + json_map["create_at"].(string) + "',"
	}
	if json_map["modify_date"] != nil {
		stringupdate += "modify_date = '" + json_map["modify_date"].(string) + "',"
	}
	if json_map["modify_at"] != nil {
		stringupdate += "modify_at = '" + json_map["modify_at"].(string) + "',"
	}

	stringupdate = trimLastChar(stringupdate)
	sqlStatement := `
	UPDATE bill
	SET ` + stringupdate + `
	WHERE id = ?`
	rows, err := DB.Query(sqlStatement, json_map["id"])
	// INSERT INTO admin_customer(customer_id, customer_name, customer_address, inline_sequence, line, phone_number_1, phone_number_2,phone_number_3,latitude,longitude,create_date,create_at,modify_date,modify_at) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)", json_map["customer_id"], json_map["customer_name"], json_map["customer_address"], json_map["inline_sequence"], json_map["line"], json_map["phone_number_1"], json_map["phone_number_2"], json_map["phone_number_3"], json_map["latitude"], json_map["longitude"], json_map["create_date"], json_map["create_at"], json_map["modify_date"], json_map["modify_at"]
	if err != nil {
		panic(err)
	}
	defer rows.Close()

	for rows.Next() {
		var id int
		var transmission_line string
		var bill_number string
		var customer_id string
		var customer_name string
		var date_sent string
		var day_count string
		var employee_name string
		var create_date string
		var create_at string
		var modify_date string
		var modify_at string
		var transmission_details string
		var employee_id string

		rows.Scan(&id,
			&transmission_line,
			&bill_number,
			&customer_id,
			&customer_name,
			&date_sent,
			&day_count,
			&employee_name,
			&create_date,
			&create_at,
			&modify_date,
			&modify_at,
			&transmission_details,
			&employee_id,
		)
		elements := make(map[string]interface{})
		elements["id"] = id
		elements["transmission_line"] = transmission_line
		elements["bill_number"] = bill_number
		elements["customer_id"] = customer_id
		elements["customer_name"] = customer_name
		elements["date_sent"] = date_sent
		elements["day_count"] = day_count
		elements["employee_name"] = employee_name
		elements["create_date"] = create_date
		elements["create_at"] = create_at
		elements["modify_date"] = modify_date
		elements["modify_at"] = modify_at
		elements["transmission_details"] = transmission_details
		elements["employee_id"] = employee_id
		array = append(array, elements)
	}

	res := make(map[string]interface{})
	res["data"] = array
	return res["data"]
}
func Addqc_pending_checklist(c echo.Context) interface{} {
	json_map := make(map[string]interface{})
	err := json.NewDecoder(c.Request().Body).Decode(&json_map)
	Initialize()
	// json_map := make(map[string]interface{})

	rows, err := DB.Exec("INSERT INTO `qc_pending_checklist`(`qcbillingtime`, `lineqc`, `qccustomercode`, `qccustomername`, `qcbillnumber`, `qcnote`, `create_date`, `create_at`, `modify_date`, `modify_at`, `qc_status`) VALUES (?,?,?,?,?,?,?,?,?,?,?)", json_map["qcbillingtime"], json_map["lineqc"], json_map["qccustomercode"], json_map["qccustomername"], json_map["qcbillnumber"], json_map["qcnote"], json_map["create_date"], json_map["create_at"], json_map["modify_date"], json_map["modify_at"], json_map["qc_status"])
	if err != nil {
		panic(err)
	}
	lastID, err := rows.LastInsertId()
	println("The last inserted row id:", lastID)
	if err != nil {
		panic(err)
	}

	res := make(map[string]interface{})

	res["data"] = lastID
	return res["data"]
}
func Deletebill(c echo.Context) interface{} {
	json_map := make(map[string]interface{})
	err := json.NewDecoder(c.Request().Body).Decode(&json_map)
	Initialize()

	sqlStatement := `
	DELETE FROM bill
	WHERE id = ?`
	rows, err := DB.Query(sqlStatement, json_map["id"])
	if err != nil {
		panic(err)
	}
	defer rows.Close()

	res := make(map[string]interface{})
	res["status"] = true

	return res
}
func trimLastChar(s string) string {
	r, size := utf8.DecodeLastRuneInString(s)
	if r == utf8.RuneError && (size == 0 || size == 1) {
		size = 0
	}
	return s[:len(s)-size]
}
