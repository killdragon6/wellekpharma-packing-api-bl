package main

import (
	"main/handler"
	"net/http"

	_ "github.com/go-sql-driver/mysql"

	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
)

func main() {

	e := echo.New()
	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{"*"},
	}))
	e.GET("/", func(c echo.Context) error {
		return c.String(http.StatusOK, "Hello, World!")
	})

	// Bl
	e.POST("/addBlPageS", handler.Addbil)
	e.GET("/getBlallbills", handler.Getbil)
	e.POST("/getBlbillsdetails", handler.Getbilbyid)
	e.POST("/updateBlbills", handler.Updatebil)
	e.POST("/addqc_pending_checklist", handler.Addqc_pending_checklist)
	e.POST("/deletebill", handler.Deletebill)
	// Port run
	e.Logger.Fatal(e.Start(":7002"))
}
